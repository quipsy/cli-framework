/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.spi;

/**
 * Thrown to indicate that the {@link Command} failed to execute.
 *
 * Allows the caller to specify the exit status of the CLI process (default is {@code 1}).
 *
 * @author Markus KARG (karg@quipsy.de)
 */
@SuppressWarnings("serial")
public class CommandException extends Exception {

    /**
     * Constructs a CommandException with no detail message, no cause and status {@code 1}.
     */
    public CommandException() {
        // intentionally left blank
    }

    /**
     * Constructs a CommandException with the specified detail message, no cause and status {@code 1}.
     *
     * @param message the detail message, or {@code null}.
     */
    public CommandException(final String message) {
        super(message);
    }

    /**
     * Constructs a CommandException with no detail message, the specified cause, and status {@code 1}.
     *
     * @param cause the cause, or {@code null}.
     */
    public CommandException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a CommandException with the specified detail message and cause, and status {@code 1}.
     *
     * @param message the detail message, or {@code null}.
     * @param cause the cause, or {@code null}.
     */
    public CommandException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a CommandException with no detail message and cause, and the specified status.
     *
     * @param status the exit status of the CLI process.
     */
    public CommandException(final int status) {
        this.status = status;
    }

    /**
     * Constructs a CommandException with the specified detail message, no cause, and the specified status.
     *
     * @param message the detail message, or {@code null}.
     * @param status the exit status of the CLI process.
     */
    public CommandException(final String message, final int status) {
        super(message);
        this.status = status;
    }

    /**
     * Constructs a CommandException with no detail message, the specified cause, and status.
     *
     * @param cause the cause, or {@code null}.
     * @param status the exit status of the CLI process.
     */
    public CommandException(final Throwable cause, final int status) {
        super(cause);
        this.status = status;
    }

    /**
     * Constructs a CommandException with the specified detail message, cause, and status.
     *
     * @param message the detail message, or {@code null}.
     * @param cause the cause, or {@code null}.
     * @param status the exit status of the CLI process.
     */
    public CommandException(final String message, final Throwable cause, final int status) {
        super(message, cause);
        this.status = status;
    }

    /**
     * The exit status of the CLI process.
     */
    private int status = 1;

    /**
     * Returns the exit code for the CLI process.
     *
     * @return The exit code for the CLI process.
     */
    public final int getStatus() {
        return this.status;
    }

}
