/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.spi;

import java.util.Collection;

/**
 * Commands have to implement this interface.
 *
 * @author Markus KARG (karg@quipsy.de)
 */
public interface Command {

    /**
     * Returns the allowed sub-command.
     *
     * @return Allowed subcommands, or an empty {@code Collection} or {@code null}.
     */
    default Collection<Command> commands() {
        return null;
    }

    /**
     * Executes this command.
     *
     * @throws CommandException if this command fails to execute.
     */
    void execute() throws CommandException;

}
