/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.spi;

/**
 * Applications supported by the CLI Framework have to implement this interface.
 *
 * @author Markus KARG (karg@quipsy.de)
 */
public interface Application {

    /**
     * Returns the name of this application.
     *
     * @return The application's name.
     */
    String programName();

    /**
     * Returns the name of the environment variable which holds options for this application.
     *
     * @return the name of the environment variable which holds options for this application.
     */
    default String environmentVariable() {
        return this.programName() + "_OPTS";
    }

}
