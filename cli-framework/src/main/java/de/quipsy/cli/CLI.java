/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli;

import java.util.Collection;
import java.util.ServiceLoader;
import java.util.stream.StreamSupport;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.defaultprovider.EnvironmentVariableDefaultProvider;

import de.quipsy.cli.spi.Application;
import de.quipsy.cli.spi.Command;
import de.quipsy.cli.spi.CommandException;

/**
 * CLI Bootloader for the wrapped application.
 *
 * @author Markus KARG (karg@quipsy.de)
 */
public class CLI {

    @Parameter(names = { "-h", "--help" }, help = true)
    private boolean help;

    /**
     * Main entry point.
     *
     * @param args CLI arguments
     */
    public static void main(final String[] args) {
        final var builder = JCommander.newBuilder().addObject(new CLI());
        final var application = StreamSupport.stream(ServiceLoader.load(Application.class).spliterator(), false).findFirst();
        application.map(Application::programName).ifPresent(builder::programName);
        application.map(Application::environmentVariable).ifPresent(varName -> builder.defaultProvider(new EnvironmentVariableDefaultProvider(varName, "-")));
        final var jCommander = builder.build();
        ServiceLoader.load(Command.class).forEach(command -> CLI.registerCommand(jCommander, command));

        JCommander leafCommander = jCommander;
        try {
            jCommander.parse(args);

            final var rootVerb = jCommander.getParsedCommand();
            final var rootCommander = jCommander.getCommands().get(rootVerb);

            if (rootCommander == null) {
                jCommander.usage();
                System.exit(1);
            }

            leafCommander = rootCommander;
            do {
                final var subVerb = leafCommander.getParsedCommand();
                final var subCommander = leafCommander.getCommands().get(subVerb);
                if (subCommander != null)
                    leafCommander = subCommander;
                else
                    break;
            } while (true);

            final var command = (Command) leafCommander.getObjects().get(0);
            command.execute();
        } catch (final CommandException e) {
            System.err.printf("%1$s: %2$s. See '%1$s --help'.%n", leafCommander.getProgramName(), e.getMessage());
            System.exit(e.getStatus());
        } catch (final Exception e) {
            System.err.printf("%1$s: %2$s. See '%1$s --help'.%n", leafCommander.getProgramName(), e.getMessage());
            System.exit(1);
        }
    }

    private static final void registerCommand(final JCommander jCommander, final Command command) {
        jCommander.addCommand(command);

        final Parameters commandParameters = command.getClass().getAnnotation(Parameters.class);
        if (commandParameters == null || commandParameters.commandNames().length == 0)
            return;
        final JCommander subCommander = jCommander.getCommands().get(commandParameters.commandNames()[0]);
        final Collection<Command> subCommands = command.commands();
        if (subCommands != null)
            subCommands.forEach(subCommand -> CLI.registerCommand(subCommander, subCommand));
    }
}
