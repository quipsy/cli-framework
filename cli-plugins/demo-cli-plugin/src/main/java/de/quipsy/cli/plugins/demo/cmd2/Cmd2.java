/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.plugins.demo.cmd2;

import java.util.Collection;
import java.util.Collections;

import com.beust.jcommander.MissingCommandException;
import com.beust.jcommander.Parameters;

import de.quipsy.cli.plugins.demo.cmd2.sub.Sub;
import de.quipsy.cli.spi.Command;

/**
 * Demo Command 2
 *
 * @author Markus KARG (karg@quipsy.de)
 */
@Parameters(commandNames = { "cmd2", "command-two" }, commandDescription = "cmd 2")
public class Cmd2 implements Command {

    @Override
    public final Collection<Command> commands() {
        return Collections.singleton(new Sub());
    }

    @Override
    public final void execute() {
        throw new MissingCommandException("Expected a command");
    }
}