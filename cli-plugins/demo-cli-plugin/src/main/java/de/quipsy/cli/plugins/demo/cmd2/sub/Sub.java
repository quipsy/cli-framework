/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.plugins.demo.cmd2.sub;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import de.quipsy.cli.spi.Command;

/**
 * Sub Command
 *
 * @author Markus KARG (karg@quipsy.de)
 */
@Parameters(commandNames = { "sub", "sub-command" }, commandDescription = "sub")
public class Sub implements Command {

    @Parameter(names = { "-v", "--verbose" }, description = "be verbose")
    private boolean verbose;

    /**
     * Demo property "verbose".
     *
     * @return {@code true} if verbosity was enabled at the command line.
     */
    public final boolean isVerbose() {
        return this.verbose;
    }

    @Override
    public void execute() {
        System.out.println("SubCommand / " + this.isVerbose());
    }
}