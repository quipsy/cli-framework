/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.plugins.demo.cmd1;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import de.quipsy.cli.spi.Command;

/**
 * Demo Command 1
 *
 * @author Markus KARG (karg@quipsy.de)
 */
@Parameters(commandNames = { "cmd1", "command-one" }, commandDescription = "cmd 1")
public class Cmd1 implements Command {

	@Parameter(description = "a")
	private String a;

	@Override
	public void execute() {
		System.out.println("First");
	}
}
